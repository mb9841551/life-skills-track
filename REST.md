# REST
REST, which stands for Representational State Transfer, is a way of organizing how computer systems on the web interact. It sets standards that help different systems communicate smoothly. When we talk about systems that follow these standards, we call them RESTful systems. One key feature of these systems is that they are stateless, meaning they don't store any information about the current state of communication. They also keep the roles of the client (user's device) and server (where data is stored) separate. This approach simplifies communication between different parts of the web.

#### Two characteristics which define REST architecture
1. ##### Separation of Client and Server
    - In the REST style of building things on the web, the client and server parts can operate independently without knowing much about each other. This means we can tweak or change the code on one side (client or server) without messing up the other side. As long as they both understand the format of the messages they exchange, we can keep them modular and separate.

    - This separation is handy because it allows us to keep user interface concerns (what users see and interact with) separate from data storage concerns (where and how data is stored). This separation makes it easier to adapt the interface for different platforms and makes the server part simpler, which helps with scalability. Plus, this split allows each part to evolve on its own.

    - With a REST interface, different clients can use the same REST endpoints (like specific web addresses), perform the same actions, and get the same responses. It's like having a common language that all the different parts of a system can understand and work with.

1. #### Statelessness
    - In systems that follow the REST approach, being stateless means that the server and client don't need to keep track of each other's current status. They can understand and handle any message they receive without needing to know about previous messages. This lack of dependency on past interactions is a result of focusing on resources rather than specific commands.

    - In simpler terms, resources are like the building blocks of the web—they represent objects, documents, or things that you might want to store or share with other services. In the world of REST, systems communicate by performing standard operations on these resources, without being tied to specific interfaces.

    - These constraints of statelessness and resource-centric communication have practical benefits. They make RESTful applications more reliable, faster, and scalable. Components can be managed, updated, and reused without causing disruptions to the entire system, even while it's up and running. It's like having a system that can adapt and grow without causing a big mess.


#### Communication between Client and Server
REST requires that a client make a request to the server in order to retrieve or modify data on the server. 

Verbs, in the context of RESTful systems, are like the methods or actions we can use to do stuff with the resources on the server. However, for newcomers to this approach, the fact that there are only a limited number of these verbs might seem a bit confusing and frustrating.

There are four main HTTP verbs which are used by well-designed RESTful systems.

- GET — retrieve a specific resource (by id) or a collection of resources
- POST — create a new resource
- PUT — update a specific resource (by id)
- DELETE — remove a specific resource by id


#### References

1. https://www.codecademy.com/article/what-is-rest
1. https://dzone.com/refcardz/rest-foundations-restful








